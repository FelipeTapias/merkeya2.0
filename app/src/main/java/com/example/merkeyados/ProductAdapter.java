package com.example.merkeyados;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.merkeyados.models.Product;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ItemViewHolder> {

    private Context context;
    private List<Product> items;
    private ProductAdapterListener listener;

    public ProductAdapter(Context context, List<Product> items, ProductAdapterListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }


    @NonNull
    //@Override
    public ProductAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View template = inflater.inflate(R.layout.activity_product_template, parent, false);
        return new ProductAdapter.ItemViewHolder(template);
    }

    //@Override
    public void onBindViewHolder(@NonNull ProductAdapter.ItemViewHolder holder, int position) {
        final Product product = items.get(position);
        holder.tvName.setText(product.getName());
        holder.tvDescription.setText(product.getDescription());
        holder.tvCantidad.setText(product.getCantidad());
        holder.tvCost.setText(product.getCost());
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.deleteUser(product.getId());
            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.editUser(product.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void updateUsers(List<Product> products) {
        this.items = products;
        notifyDataSetChanged();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvDescription;
        private TextView tvCantidad;
        private TextView tvCost;
        private ImageView ivEdit;
        private ImageView ivDelete;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvCantidad = itemView.findViewById(R.id.tv_cantidad);
            tvCost = itemView.findViewById(R.id.tv_cost);
            ivEdit = itemView.findViewById(R.id.iv_edit);
            ivDelete = itemView.findViewById(R.id.iv_delete);
        }
    }

    public interface ProductAdapterListener {

        void deleteUser(String id);

        void editUser(String id);
    }
}




