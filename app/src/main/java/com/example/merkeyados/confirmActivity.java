package com.example.merkeyados;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.merkeyados.models.Product;
import io.realm.Realm;
import io.realm.RealmModel;


public class confirmActivity extends AppCompatActivity {

    private Realm realm;
    private Product product;
    private TextView tvName;
    private TextView tvDescription;
    private TextView tvCantidad;
    private TextView tvCost;

    private Button btnConfirm;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);
        realm = Realm.getDefaultInstance();
        loadToolbar();
        loadViews();

        product = (Product) getIntent().getSerializableExtra("product");
        loadUser();
    }



    private void loadUser() {
        tvName.setText(product.getName());
        tvDescription.setText(product.getDescription());
        tvCantidad.setText(product.getCantidad());
        tvCost.setText(product.getCost());
    }

    private void loadViews() {
        tvName = findViewById(R.id.tv_name);
        tvDescription = findViewById(R.id.tv_description);
        tvCantidad = findViewById(R.id.tv_cantidad);
        tvCost = findViewById(R.id.tv_cost);
        btnConfirm = findViewById(R.id.btn_confirm);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmProduct();
            }
        });
    }

    private void confirmProduct() {
        Toast.makeText(this, "Usuario confirmado", Toast.LENGTH_SHORT).show();
        addUserInDB(product);
    }

    private void addUserInDB(Product product) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate((Iterable<RealmModel>) product);
        realm.commitTransaction();
    }


    private void loadToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.confirmTitle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}